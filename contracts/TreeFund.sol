// contracts/TreeFund.sol
pragma solidity ^0.6.4;

/// @title An on-chain fund that collects, stores, invests, and distributes coins for benefactors 
/// @author Zachary Wolff @WolfDeFi
/// @notice Seed your tree fund for friends & family  
/// @dev This contract has not been audited. While there are a limited number of attack vetors, use at your own risk. 

//brownie
import "OpenZeppelin/openzeppelin-contracts@3.3.0/contracts/token/ERC20/IERC20.sol";
import "OpenZeppelin/openzeppelin-contracts@3.3.0/contracts/access/Ownable.sol";
import "OpenZeppelin/openzeppelin-contracts@3.3.0/contracts/math/SafeMath.sol";

contract TreeFund is Ownable {
    using SafeMath for uint; 
    using SafeMath for uint256; 
    
    /// @dev this is the TreeFund config 
    // active - is this config active? (only one config should ever be active at a time)
    // open - is the fund open for leaf withdrawls default = 0, not open. otherwise set to future unix date
    // max_wd - max withdrawal size, not currently used 
    struct TreeConf {
        bool active;
        uint open;
        uint max_wd;
    }

    /// @dev versionable config
    // where uint = config number 
    mapping(uint => TreeConf) private treeFundConf;

    // use this array as index to track all leaves 
    address[] public leafIndex; 
    
    /// @dev Each leaf node will have an assoiciated struct
    // created = unix time the leafNode joined (can be backdated) 
    // active = is the leafNode active or not? Inactive nodes cannot withdrawl funds
    // withdrawn (MCD => 1.15) - % of entitlment a given leaf has withdrawn  
    struct Leaf {
        uint created;
        bool active;
        mapping (string => uint) leafWithdraw; 
    }

    // now we map leaf addresses to thier leaf struct
    mapping (address => Leaf) private leafNodes; 

    // create index for funded coins 
    string[] private coinIndex; 

    // mapping for storing funded coins and their contract addresses
    mapping (string => address) private coinAddresses; 

    /// @dev treeState is the source from which we will derive leaf node balances 
    /// treeState is a key tracking mechanism that represents  
    /// the total amount of time (in seconds) that all leaves have been in the fund.  
    uint public treeState; 

    // so we can know when the last time the treeState was updated 
    uint public treeStateLastUpdated; 

    // Events - publicize actions to external listeners
    
    // Base contract events
    event DepositETHLog(uint value);
    event Received(address, uint); 
    event Fallback(address, uint);

    // Leaf Events
    event AddedLeaf(address, uint); 
    event UpdatedLeafActive(address, bool);
    event UpdatedLeafCreation(address, uint);
    event TreeStateUpdated(uint previousTreeState, uint treeState, uint previousTreeStateUpdated, uint treeStateLastUpdated);  
    event GetLeafTree(uint percent_entitled);

    // CoinMapping Events
    event AdddedCoinMapping(address, string);
    event UpdateCoinMapping(address, string);

    // leaf node withdraw events
    event LogLeafWithdraw(address leaf, string coin, uint256 amount, uint leaf_limit, uint percent); 

    // debugging events that will likely be removed 
    event LogBalance(address, uint);
    event Difference(uint difference);
    event UpdatedLeafCreationLog(uint, uint);

    /// @dev future versions will likely accept some of these params on contract creation 
    constructor() public payable{

        // initalize Tree State 
        treeStateLastUpdated = now; 

        // add your starting leaf node public addys (min = 1) 
        addLeaf(0xD0896e012907d9938BE5d3fBc8e9FF5d284E2214);
        addLeaf(0xBf03c3354aae2B14731838fCb7BD322b11685689);

        // add inital coin mapping for Ethereum token support (required)   
        addCoinMapping("ETH", 0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE);

        // add coin mapping for any ERC20 coins you want to fund (optional)
        // for example, as I intented to deposit DAI - mainnet MCD DAI contract address
        // new mappings can be added any time via the admin interface of the contract
        addCoinMapping("MCD", 0x6B175474E89094C44Da98b954EedeAC495271d0F);

        // establish tree fund configuation 
        treeFundConf[0].active = true;
        treeFundConf[0].open = 1590527335; // unix time stamp when fund will open for withdraw 
        treeFundConf[0].max_wd = 50; 

    }

    /// @notice Catches any incoming calls that don't match a function & don't contain Eth
    fallback() external {
    }

    /// @notice Catches any incoming tx's that dont match a function & do contain Eth 
    receive() external payable {
        // check to see if we need to adjust leaf percentages? 
        emit Received(msg.sender, msg.value);
    }  

    /// @notice this function is used to add new leaf nodes 
    function addLeaf(address payable _leaf_node_address) public onlyOwner {
        // Leaf Nodes must be unique.  
        require(leafNodes[_leaf_node_address].created == 0, "Leaf already exists!");
        
        // Set leaf node created time and active flag 
        leafNodes[_leaf_node_address].created = now;
        leafNodes[_leaf_node_address].active = true; 
        // leafNodes[_leaf_node_address].leafWithdraw["ETH"] = 1000; // %/100 for now for two decimal % support  

        // do this before we bump the leafIndex counter        
        updateTreeState();  
        leafIndex.push(_leaf_node_address);
      
        emit AddedLeaf(_leaf_node_address, leafIndex.length);

    }

    /// @notice this admin function is used to backdate leaf node creation time
    /// this allows you to effectively adjust a given leaf node % of entitlement
    /// @param _leaf_node_address The account/address for the new leaf 
    /// @param _created_time unix time at which the leaf be included in the pool 
    function updateLeafCreationTime(address _leaf_node_address, uint _created_time) public onlyOwner {
        // used for debugging ES
        emit UpdatedLeafCreationLog(treeStateLastUpdated, leafNodes[_leaf_node_address].created);
        uint previousTreeState = treeState;
        uint previousTreeStateUpdated = treeStateLastUpdated; 
        
        // 1) remove any time that has been added for this leaf 
        // if leaf was last one added and no other functions have trigged update to ES, this will eval to false
        if (treeStateLastUpdated > leafNodes[_leaf_node_address].created) { 
            // how much time has leaf contributed to treeState? 
            uint oldLeafTimeinTotal = now - leafNodes[_leaf_node_address].created;
            // now subtract that ^^ from total pool 
            treeState = treeState - oldLeafTimeinTotal;
        }

        // 2) calc time leaf has been in fund/pool based on new created time and add it to treeState
        uint newLeafTimeinTotal = now - _created_time; 
        /// and add that to the fund treeState
        treeState = treeState + newLeafTimeinTotal;

        // step 3 - as we're going to need to update the treeStateLastUpdated = now; 
        // we must also make sure all other leafs get thier time added too
        uint leafCount = leafIndex.length; 

        // how much time has passed since we last updated the treeState? 
        uint difference = now - treeStateLastUpdated; 
        // used for debugging, can remove for prod 
        emit Difference(difference);
        // any leaves that have accrued time since the last update that ARENT the leaf being updated (hence -1), because that's covered with #2 ^^ 
        treeState = treeState + (difference * (leafCount -1));

        // reset our state tracker for total leaf times 
        treeStateLastUpdated = now;

        // finally, update the created time for the leaf 
        leafNodes[_leaf_node_address].created = _created_time;

        // used for debugging, can probably remove in prod 
        emit TreeStateUpdated(previousTreeState, treeState, previousTreeStateUpdated, treeStateLastUpdated);
        emit UpdatedLeafCreation(_leaf_node_address, _created_time);
    }

    /// @notice this function will return the length of the leafIndex array
    function getLeafIndexLength() public view returns (uint) {
        return (leafIndex.length);
    }

    /// @notice this function is used to update the treeState
    function updateTreeState() private { 

        // used for debugging 
        uint previousTreeState = treeState;
        uint previousTreeStateUpdated = treeStateLastUpdated; 

        // how much time has passed since we last updated the treeState? 
        uint difference = now - treeStateLastUpdated; 
        // used for debugging, can remove for prod 
        emit Difference(difference);
        // if there is no difference in treeStateLastUpdated then there is no need to update 
        if (difference != 0) { 
            // add any time that has passed since last update for all leaves! 
            treeState = treeState + (difference * leafIndex.length);
            treeStateLastUpdated = now;
            // this emit _could_ be removed in prod to reduce gas costs if desired 
            emit TreeStateUpdated(previousTreeState, treeState, previousTreeStateUpdated, treeStateLastUpdated);
        }


    }

    /// @notice this function is used to update leaf node active flag.
    // the idea here is that an inactive node will not be able to withdraw and/or be considered for % calculations of the fund 
    function updateLeafActive(address _leaf_node_address, bool _active) public onlyOwner {
        leafNodes[_leaf_node_address].active = _active;
        emit UpdatedLeafActive(_leaf_node_address, _active);
    }

    /// @notice this function can be used to retrieve leaf node data 
    function getLeaf(address _leaf_node_address) public view onlyOwner returns(uint created, bool active){ 
        created = leafNodes[_leaf_node_address].created;
        active = leafNodes[_leaf_node_address].active;
    }

    /// @notice used to return all leafnode addresses 
    function getAllLeaves() external view returns (address[] memory) {
        return leafIndex;
    }
    
    /// @notice this function is used to add new funded ERC20 coin mappings 
    /// @param _coin_name like MCD or xDAI - String=>Address mapping | MCD=>0x4242
    /// @param _contract_address contract address for ERC20 contract of the coin 
    function addCoinMapping(string memory _coin_name, address payable _contract_address) public onlyOwner {
         // set coin name and mapped address
         coinAddresses[_coin_name] = _contract_address;

         // add new coin to our coinIndex, do not remove until we upgrade coin mapping method
         coinIndex.push(_coin_name);
         emit AdddedCoinMapping(_contract_address, _coin_name);
    }

    /// @notice used to return coin mapping. might need to create an index for this too
    function getCoinMapping(string memory _coin_name) public view returns (address){ 
        return coinAddresses[_coin_name];   
    }
    
    /**TypeError: This type is only supported in ABIEncoderV2. Use "pragma experimental ABIEncoderV2;" to enable the feature
    /// @notice used to return all coin names the contract has mapped/stored 
    function getAllcoinMappings() external view returns (string[] memory) {
        return coinIndex;
    }
    */
    
    /// @notice can be used to update a coin name to address mapping, in the event that contract address changes
    /// @param _coin_name coin name like xDAI or MCD 
    /// @param _contract_address for the ERC20 being updated 
    function updateCoinMapping (string memory _coin_name, address payable _contract_address) public onlyOwner { 
        coinAddresses[_coin_name] = _contract_address; 
        emit UpdateCoinMapping (_contract_address, _coin_name);
    }

    /// @notice this function is used to calculate & return leaf entitlement %
    /// @param _leaf_node_address addres of the leaf node you want to check % of entitlment for 
    // https://stackoverflow.com/questions/42738640/division-in-ethereum-solidity/42739843
    function getLeafTree(address _leaf_node_address) public returns (uint quotient) { 
        // require msg.sender is a leaf node, revert if not  
        require(leafNodes[_leaf_node_address].active, "Address is not valid leaf");

        // re-entrancy bug/attack? todo: add require() here maybe
        updateTreeState(); // make sure our state is up to date! 

        // how much time has leafnode been in the pool/fund?
        uint numerator = now - leafNodes[_leaf_node_address].created;
        uint denominator = treeState;  
        uint precision = 4;

        // caution, check safe-to-multiply here
        uint _numerator  = numerator * 10 ** (precision+1);
        // with rounding of last digit
        uint _quotient =  ((_numerator / denominator) + 5) / 10;
        emit GetLeafTree(_quotient);
        return ( _quotient);
    }

    /// @notice This method can be used to deposit ETH into the fund. Likely obsolete at this point  
    function deposit_ETH() public payable { 
        uint value = msg.value;
        emit DepositETHLog(value);
    }

    /// @notice This function is used to get this contracts balance of a given ERC20 coin
    /// @param _erc_contract_address contract address for ERC20 in question  
    function getERC20balance(address _erc_contract_address) public view returns(uint256) {
        IERC20 token = IERC20(_erc_contract_address);
        return (token.balanceOf(address(this)));    
    }
    /// @notice This function can be used to see what % of entitled a given token has withdrawn 
    function getLeafWithdraw(address _leaf_address, string memory _coin_name) public view returns(uint256) {
        return leafNodes[_leaf_address].leafWithdraw[_coin_name];
    }

    /// @notice This function is designed for contract owner to transfer ERC20 coins out
    /// @dev If you want this contract to be immuatable (no admin backdoor) you can remove this
    /// please make sure leaves can withdraw ERC20s before you remove this! 
    function withdrawERC20(
        address _erc20Contract, 
        address payable recipient, 
        uint256 tokenAmount
        ) public onlyOwner { 
        IERC20 token = IERC20(_erc20Contract);
        token.transfer(recipient, tokenAmount);
    }

    /// @notice This method is used by leaves to withdraw coins from the fund 
    function leafWithdrawRequest(string memory _coin_name, uint256 _token_amount) public {

        uint256 current_balance = 0;
        // step 1 : require msg.sender is a leaf node, revert if not  
        bool leaf_active = leafNodes[msg.sender].active;
        require(leaf_active, "Address is not valid leaf");
          
        // step 2: validate global treeFundConf 
        require(treeFundConf[0].active, "No active config found!");
        require(treeFundConf[0].open < now, "Fund not open for withdrawals!"); // is the fund open for WD?
        
        // step 3: check user specfic withdraw limits 
        // confirm global entitiled state is > 0
        uint leaf_percent_entitled = getLeafTree(msg.sender); // this will be the total % of fund they are entitled too
        require(leaf_percent_entitled > 0, "TreeFund - No availible entitlment"); // 10k means leaf has drained this assest! because 100.00 == 100%?
        
        // confirm that our token wd tracker for _coin_name is not at 100% yet, (10000) 
        uint token_wd_percent = leafNodes[msg.sender].leafWithdraw[_coin_name]; // what % have they already withdrawn for this coin? will be 0 if nothing has been withdrawn 
        require(token_wd_percent <= 100, "Leaf limit % exceeded for this token!"); // if leaf has already withdrawn 100% for this coin, revert 
        
        // step 4: get _coin_name balance from the contract 
        if (keccak256(abi.encodePacked(_coin_name)) == keccak256("ETH")) {
            current_balance = address(this).balance;
        } else { 
            current_balance = getERC20balance(coinAddresses[_coin_name]);
        } 
        require(current_balance > _token_amount, "TreeFund - Not enough coins to fulifll your request");
 
        // max amount the leaf could wd right now 
        uint256 leaf_max_wd = (leaf_percent_entitled * current_balance) / 100;
 
        // confirm requested withdraw is <= leaf_max_wd
        require(_token_amount <= leaf_max_wd, "TreeFund - Request is > than allowed");

        // what % of the current_balance does the requested withdraw amount represent? result = _quotient 
        uint numerator = _token_amount;
        uint denominator = leaf_max_wd;  
        uint precision = 4;
        uint _numerator  = numerator * 10 ** (precision+1); // caution, check safe-to-multiply here
        uint _quotient =  ((_numerator / denominator) + 5) / 10; // with rounding of last digit

        // step 4: account for withdraw by updating leaf node % entitled for coin in question 
        leafNodes[msg.sender].leafWithdraw[_coin_name] = token_wd_percent + _quotient;   
        // require(leafNodes[msg.sender].leafWithdraw[_coin_name] > leaf_limit, "There was an issue deducting from leaf state!"); // primitive check, needs to be improved

        // step 5: finally, send coins
        if (keccak256(abi.encodePacked(_coin_name)) == keccak256("ETH")) {
            msg.sender.transfer(_token_amount);
        } else {
            IERC20 token = IERC20(coinAddresses[_coin_name]);
            token.transfer(msg.sender, _token_amount);
        }

        LogLeafWithdraw(msg.sender, _coin_name, _token_amount, token_wd_percent, leaf_percent_entitled);
        
        
    }
    
  
}
