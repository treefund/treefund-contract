# -*- coding: utf-8 -*-

from brownie import *
from brownie import accounts, web3
import time
import sys

# brownie run deploy-basic.py --gas  
# or better yet, open console first, then run: run('deploy-basic')
# for individual functions (after initial deploy) - run('deploy-basic', 'foo_bar') 

# set multisig address to be that of our first account. For production you will want to change this to the correct multisig addy 
owner = accounts[0]


def main():
    # print out some relevant info about our testing env 
    loginfo()
    # deploy contract 
    try:
        tf = TreeFund.deploy({'from': accounts[0]})
    except:
        # todo: need to update to catch specific exceptions
        e = sys.exc_info()[0]
        print(e)

    # send some ETH to the contract 
    accounts[0].transfer(tf.address, "10 ether")
    get_leaf_withdraw_percent(tf, 1, "ETH")
    place_withdraw(tf, 1, "ETH", 1000000000000000000)
    # get_leaf_withdraw_percent(tf, 1, "ETH")


def get_leaf_withdraw_percent(tf, leaf_number, coin_name):
    leaf_1_withdraw_percentage_tx = tf.getLeafWithdraw(accounts[leaf_number], coin_name)
    print(f'leaf {leaf_number} withdraw percentage for coin {coin_name} = {leaf_1_withdraw_percentage_tx}')

def place_withdraw(tf, leaf_number, coin_name, amount):
    wd1 = tf.leafWithdrawRequest(coin_name, amount, {'from': accounts[leaf_number]})
    print(f'Withdraw results {wd1}')

def loginfo():
    print(f"\nWeb3 Provider URL: {web3.provider.endpoint_uri}\n")

    for account_number in range(9):
        print(f"account #: {account_number} - {accounts[account_number]}")
    
    print("\n")    
    # ETH 
